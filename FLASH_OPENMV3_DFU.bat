@echo off
pushd %~dp0

set PATH=%CD%;%PATH%

set DFU="%1"
if "%1" == "" set DFU=build-OPENMV3-stm32\firmware.dfu
if NOT EXIST "%DFU%" set DFU=firmware.dfu

py -m pipenv install

py -m pipenv run python micropython\tools\pydfu.py --upload %DFU%

pause
popd