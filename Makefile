# Out of tree micropython builds

# Select the board to build for: if not given on the command line,
BOARD ?= OPENMV3

# unix is also a working option
PORT ?= stm32

# If the build directory is not given, make it reflect the board name.
BUILD := $(abspath build-$(BOARD)-$(PORT))

BOARD_DIR := $(abspath omv/boards/$(BOARD)/)

ifneq ($(wildcard $(BOARD_DIR)/mpconfigboard.mk),)
include $(BOARD_DIR)/mpconfigboard.mk
endif

# Path relative to the micropython port folder
export USER_C_MODULES = ../../..

export V
export DEBUG

MICROPYTHON_ = -C micropython/ports/$(PORT) PORT=$(PORT) BUILD=$(BUILD) BOARD=$(BOARD) BOARD_DIR=$(BOARD_DIR) CFLAGS_EXTRA=$(CFLAGS_EXTRA)

all: $(PORT)

stm32:
	$(MAKE) $(MICROPYTHON_) all

unix:
	$(MAKE) $(MICROPYTHON_) deplibs
	$(MAKE) $(MICROPYTHON_) CFLAGS_EXTRA="-I$(BOARD_DIR) $(CFLAGS_EXTRA) -DMP_CONFIGFILE='<mpconfigunix.h>'" PROG=$(BUILD)/micropython all
	#$(MAKE) $(MICROPYTHON_) CFLAGS_EXTRA="$(CFLAGS_EXTRA) -I$(BOARD_DIR)" PROG=$(BUILD)/micropython_cov coverage

clean:
	$(MAKE) $(MICROPYTHON_) clean
